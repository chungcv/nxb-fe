﻿var datasource = [
    [
        [
            {
                name: "Sách giáo khoa",
                data: [2000, 1300, 2600, 1100]
            }, {
                name: "Sách bổ trợ",
                data: [1000, 700, 1300, 600]
            }, {
                name: "Sách tham khảo",
                data: [500, 370, 800, 250]
            }
        ]
        , [{
            name: "Sách giáo khoa",
            data: [14000, 9100, 18200, 7700]
        }, {
            name: "Sách bổ trợ",
            data: [7000, 4900, 9100, 4200]
        }, {
            name: "Sách tham khảo",
            data: [3500, 2590, 5600, 1750]
        }]
        , [{
            name: "Sách giáo khoa",
            data: [60000, 39000, 78000, 33000]
        }, {
            name: "Sách bổ trợ",
            data: [30000, 21000, 39000, 18000]
        }, {
            name: "Sách tham khảo",
            data: [15000, 11100, 24000, 7500]
        }]
        , [{
            name: "Sách giáo khoa",
            data: [240000, 156000, 312000, 132000]
        }, {
            name: "Sách bổ trợ",
            data: [120000, 84000, 156000, 72000]
        }, {
            name: "Sách tham khảo",
            data: [60000, 44400, 96000, 30000]
        }]
        , [{
            name: "Sách giáo khoa",
            data: [730000, 474500, 949000, 401500]
        }, {
            name: "Sách bổ trợ",
            data: [365000, 255500, 474500, 219000]
        }, {
            name: "Sách tham khảo",
            data: [182500, 135050, 292000, 91250]
        }]
    ]
    , [
        [
            {
                name: "Sách giáo khoa",
                data: [100, 80, 150, 60]
            }, {
                name: "Sách bổ trợ",
                data: [150, 120, 170, 90]
            }, {
                name: "Sách tham khảo",
                data: [70, 50, 90, 30]
            }
        ]
        , [{
            name: "Sách giáo khoa",
            data: [700, 560, 1050, 420]
        }, {
            name: "Sách bổ trợ",
            data: [1050, 840, 1190, 630]
        }, {
            name: "Sách tham khảo",
            data: [490, 350, 630, 210]
        }]
        , [{
            name: "Sách giáo khoa",
            data: [3000, 2400, 4500, 1800]
        }, {
            name: "Sách bổ trợ",
            data: [4500, 3600, 5100, 2700]
        }, {
            name: "Sách tham khảo",
            data: [2100, 1500, 2700, 900]
        }]
        , [{
            name: "Sách giáo khoa",
            data: [12000, 9600, 18000, 7200]
        }, {
            name: "Sách bổ trợ",
            data: [18000, 14400, 20400, 10800]
        }, {
            name: "Sách tham khảo",
            data: [8400, 6000, 10800, 3600]
        }]
        , [{
            name: "Sách giáo khoa",
            data: [36500, 29200, 54750, 21900]
        }, {
            name: "Sách bổ trợ",
            data: [54750, 43800, 62050, 32850]
        }, {
            name: "Sách tham khảo",
            data: [25550, 18250, 32850, 10950]
        }]
    ]
]

var dataUser = [
    ['Bùi Kim Quyên', 'Region A', 200, 500, 1000]
    , ['Võ An Phước', 'Region B', 180, 700, 1300]
    , ['Phạm Nguyễn QuỳnhTrân', 'Region A', 150, 600, 2300]
    , ['Dương Hoài Phương', 'Region C', 130, 900, 4100]
    , ['Phan Vinh Bính', 'Region C', 125, 200, 5300]
    , ['Võ Minh Thư', 'Region C', 122, 1200, 3300]
    , ['Phan Huỳnh Ngọc Dung', 'Region D', 122, 500, 2100]
    , ['Nguyễn Vân Anh', 'Region B', 90, 400, 1300]
    , ['Nguyễn Thế Vinh', 'Region C', 80, 700, 6000]
    , ['Nguyen Thi Thanh Bích', 'Region C', 70, 600, 9000]
]

var time = 0;
var sum = 0;

$(function () {
    createChartBar(datasource[0][0], "VND");
    createChartCircle(datasource[0][0], "VND");
    createChartLine("week");

    $(document).bind("kendo:skinChange", createChartBar);
    $(document).bind("kendo:skinChange", createChartCircle);
    $(document).bind("kendo:skinChange", createChartLine);
    $(".js-select-time-type").kendoMobileButtonGroup({
        select: function (e) {
            time = e.index;
            refresh();
        },
        index: 0
    });
    $(".js-select-sum-type").kendoMobileButtonGroup({
        select: function (e) {
            sum = e.index;
            refresh();
        },
        index: 0
    });

    $(".js-select-time-line-type").kendoMobileButtonGroup({
        select: function (e) {
            createChartLine(e.index);
        },
        index: 0
    });

    $(".js-select-buy-top").kendoMobileButtonGroup({
        select: function (e) {
            createBuyTop(e.index);
        },
        index: 0
    });

    createBuyTop(0);

    $("#grid").kendoGrid({
        height: 550,
        sortable: true
    });
});

function refresh() {
    series = datasource[sum][time];
    createChartBar(series, sum);
    createChartCircle(series, sum);

}

function createBuyTop(index) {
    $(".js-table-buy-top").html('');
    $.each(dataUser, function (key, value) {
        $(".js-table-buy-top").append('<tr>'
            + '<td>' + value[0] + '</td>'
            + '<td>' + value[1] + '</td>'
            + '<td>' + value[2 + index] + '</td>'
            + '</tr>'
        );
    });
}

function createChartBar(series, unit) {
    if (unit == 0) {
        unit = 'VND';
    } else {
        unit = 'Quyển';
    }

    $(".js-chart-bar").kendoChart({
        title: {
            text: "Doanh thu nhà xuất bản"
        },
        legend: {
            position: "top"
        },
        seriesDefaults: {
            type: "column"
        },
        series: series,
        valueAxis: {
            labels: {
                format: "{0} " + unit
            },
            line: {
                visible: false
            },
            axisCrossingValue: 0
        },
        categoryAxis: {
            categories: ['KV 1', 'KV 2', 'KV 3', 'KV 4'],
            line: {
                visible: false
            },
            labels: {
                padding: { top: 5 }
            }
        },
        tooltip: {
            visible: true,
            format: "{0} " + unit,
            template: "#= series.name #: #= value #"
        }
    });
}

function createChartCircle(series, unit) {
    if (unit == 0) {
        unit = 'VND';
    } else {
        unit = 'Quyển';
    }
    let sgk = series[0].data[0] + series[0].data[1] + series[0].data[2] + series[0].data[3];
    let sbt = series[1].data[0] + series[1].data[1] + series[1].data[2] + series[1].data[3];
    let stk = series[2].data[0] + series[2].data[1] + series[2].data[2] + series[2].data[3];
    let sum = sgk + sbt + stk;
    let sgkP = sgk / sum * 100;
    let sbtP = sbt / sum * 100;
    let stkP = 100 - sgkP - sbtP;

    $(".js-chart-circle").kendoChart({
        title: {
            text: "Tỉ lệ các loại sách"
        },
        legend: {
            position: "top"
        },
        seriesDefaults: {
            labels: {
                template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                position: "outsideEnd",
                visible: true,
                background: "transparent"
            }
        },
        series: [{
            type: "pie",
            data: [{
                category: "Sách giáo khoa",
                value: sgkP
            }, {
                category: "Sách bổ trợ",
                value: sbtP
            }, {
                category: "Sách tham khảo",
                value: stkP
            }]
        }],
        tooltip: {
            visible: true,
            template: "#= category # - #= kendo.format('{0:P}', percentage) #"
        }
    });
}
function createChartLine(unit) {
    let data = [];
    let columnName = [];

    if (unit == 0) {
        let base = 1000;
        for (let i = 2; i <= 7 ; i++) {
            columnName.push('Thứ ' + i);
            base = base + Math.floor((Math.random() * 100) + 1) - 50;
            data.push(base);
        }
        columnName.push('Chủ nhật');
        base = base + Math.floor((Math.random() * 100) + 1) - 50;
        data.push(base);
    } else if (unit == 1) {
        let base = 4000;
        for (let i = 1; i <= 30 ; i++) {
            columnName.push(i);
            base = base + Math.floor((Math.random() * 100) + 1) - 50;
            data.push(base);
        }
    } else {
        let base = 48000;
        for (let i = 1; i <= 12 ; i++) {
            columnName.push('Tháng ' + i);
            base = base + Math.floor((Math.random() * 100) + 1) - 50;
            data.push(base);
        }
    }

    $(".js-chart-line").kendoChart({
        title: {
            text: "Biểu đồ doanh thu"
        },
        legend: {
            position: "bottom"
        },
        chartArea: {
            background: ""
        },
        seriesDefaults: {
            type: "line",
            style: "smooth"
        },
        series: [{
            name: "Doanh thu",
            data: data
        }],
        valueAxis: {
            labels: {
                format: "{0} VND"
            },
            line: {
                visible: false
            },
            axisCrossingValue: -10
        },
        categoryAxis: {
            categories: columnName,
            majorGridLines: {
                visible: false
            },
            labels: {
                rotation: "auto"
            }
        },
        tooltip: {
            visible: true,
            format: "{0} VND",
            template: "#= series.name #: #= value #"
        }
    });
}