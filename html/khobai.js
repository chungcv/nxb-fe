var dataProduct = [
    ['Đơn Hàng 1','Sách giáo khoa lớp 1',1000,500,500,'Miền Bắc']
    , ['Đơn Hàng 2','Sách bổ trợ lớp 2',1500,2000,0,'']
    , ['Đơn Hàng 3','Sách tham khảo lớp 3',2000,1600,0,'']
    , ['Đơn Hàng 4','Sách giáo khoa lớp 5',200,100,100,'Miền Nam']
]

var dataSum = [
    [
        [10,30,100]
        , [1000, 5000, 10000]
        , [3000, 15000, 30000]
    ]
    , [
        [2, 5, 10]
        , [300, 500, 1000]
    ]
    , [
        [10, 40, 70]
        , [1000, 2000, 3000]
        , [10, 20, 30]
    ]
]


$(function() {
    
    $(".js-select-region").kendoMobileButtonGroup({
        select: function(e) {
        },
        index: 0
    });

    $(".js-select-sum").kendoMobileButtonGroup({
        select: function(e) {
            changeSum(e.index);
        },
        index: 0
    });
    
    $("#vertical").kendoSplitter({
        orientation: "vertical",
        panes: [
            { collapsible: false },
            { collapsible: false, size: "100px" },
            { collapsible: false, resizable: false, size: "100px" }
        ]
    });

    $("#horizontal").kendoSplitter({
        panes: [
            { collapsible: true },
            { collapsible: false },
            { collapsible: true}
        ]
    });

    $(".js-table-product").html('');
    $.each(dataProduct, function (key, value) {

        if (key === 0 || key === 3) {
            $(".js-table-product").append('<tr>'
                + '<td>' + value[0] + '</td>'
                + '<td>' + value[1] + '</td>'
                + '<td><span class="up-to-red">' + value[2] + '</span></td>'
                + '<td>' + value[3] + '</td>'
                + '<td>' + value[4] + '</td>'
                + '<td>' + value[5] + '</td>'
                + '</tr>'
            );
        } else {
            $(".js-table-product").append('<tr>'
                + '<td>' + value[0] + '</td>'
                + '<td>' + value[1] + '</td>'
                + '<td>' + value[2] + '</td>'
                + '<td>' + value[3] + '</td>'
                + '<td>' + value[4] + '</td>'
                + '<td>' + value[5] + '</td>'
                + '</tr>'
            );
        }

        $(".js-table-product").append('<tr>'
            + '<td>' + value[0] + '</td>'
            + '<td>' + value[1] + '</td>'
            + '<td>' + key == 0 ? ('<span class="up-to-red">' + value[2] + "</span>" )  : value[2] + '</td>'
            + '<td>' + value[3] + '</td>'
            + '<td>' + value[4] + '</td>'
            + '<td>' + value[5] + '</td>'
            + '</tr>'
        );
    });

    $("#grid").kendoGrid({
        sortable: true
    });
    changeSum(0);
});

function changeSum(index) {
    $(".js-sum00").html(dataSum[0][0][index]);
    $(".js-sum01").html(dataSum[0][1][index]);
    $(".js-sum02").html(dataSum[0][2][index]);
    $(".js-sum10").html(dataSum[1][0][index]);
    $(".js-sum11").html(dataSum[1][1][index]);
    $(".js-sum20").html(dataSum[2][0][index]);
    $(".js-sum21").html(dataSum[0][1][index]);
    $(".js-sum22").html(dataSum[0][2][index]);
}
