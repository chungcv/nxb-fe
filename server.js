'use strict';

const app = require('koa')();
const logger = require('koa-logger');
const serve = require('koa-static');
const router = require('koa-router');
const path = require('path');
const fs = require('fs');
const dom = require('./modules/koa-dom');
/** Not log infor of request */
//app.use(logger());
app.use(dom);

app.use(serve(path.join(__dirname, '/src')));
app.use(serve(path.join(__dirname, '/src/html')));

app.listen('8088');
console.log('listening on port 8088');
